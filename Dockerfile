FROM node:alpine
ENV PORT 80
ENV DD_SERVICE_MAPPING=next:platform-dashboard
ENV DD_RUNTIME_METRICS_ENABLED=true
ENV DD_LOGS_INJECTION=true

# Set working directory
RUN mkdir -p /message-center-frontend
WORKDIR /message-center-frontend

# Install PM2 globally
# RUN npm install --global pm2

# Copy "package.json" and "package-lock.json" before other files
COPY ./package*.json ./

# Install dependencies
RUN npm install && \
    npm cache clean --force

ENV NODE_ENV=production

# Copy all files
COPY ./ ./

# Build app
RUN npm run build

# Install SSL certificate
RUN apk update && apk add ca-certificates
# COPY ./jobtarget.pem /etc/ssl/certs
# RUN update-ca-certificates

# Expose the listening port
EXPOSE $PORT

# Run container as non-root (unprivileged) user
# The "node" user is provided in the Node.js Alpine base image
USER node

# Launch app with PM2
CMD [ "pm2-runtime", "npm", "--", "start" ]
