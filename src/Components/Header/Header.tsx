import React from 'react';
import classes from './Header.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons';

const Header: React.FC = () => (
    <nav className={classes.wrapper}>
        <div className={classes.titleStyles}>
            <FontAwesomeIcon size="2x" icon={faEnvelopeOpen}></FontAwesomeIcon>
            <span className={classes.titleStyles}>Message Center</span>
        </div>
        <button className={classes.newMessageButton} >New Message</button>
    </nav>
);

export default Header;